import React from 'react';
import { View, Text, Button, ScrollView, Pressable } from 'react-native';
import { BleManager } from 'react-native-ble-plx';

export default class TestScreen extends React.Component {

    constructor() {
        super();
        this.manager = new BleManager();
        this.state = {
            deviceId: null,//"C8:FD:19:9B:51:79",
            servicesList: [],
            characteristicsList: [],
        }
    }

    componentDidMount() {
        const subscription = this.manager.onStateChange((state) => {
            console.log('State');
            console.log(state);
        }, true);
    }

    scan() {
        console.log('Scan');
        this.setState({
            deviceId: null,
            servicesList: [],
            characteristicsList: []
        });
        this.manager.startDeviceScan(null, null, (error, device) => {
            if (error) {
                console.log(error);
                return;
            }
            console.log(device.name);
            if (device.name === 'meter+00175277') {
                console.log('Found Device')
                this.manager.stopDeviceScan();


                device.connect().then((device) => {
                    console.log('Success');
                    console.log(device);
                    return device.discoverAllServicesAndCharacteristics();
                }).then(async (device) => {
                    console.log("DISCOVER ALL SERVICES AND CHARACTERISTICS");
                    console.log(device);
                    console.log(device.id);
                    this.setState({
                        deviceId: device.id
                    });

                    // this.manager.servicesForDevice(device.id).then((result) => {
                    //     console.log('Device Result: ', result);
                    //     if (result) {
                    //         for (let i = 0; i < result.length; i++) {
                    //             this.manager.characteristicsForDevice(result[i].deviceID, result[i].uuid).then((characteristic) => {
                    //                 console.log('characteristic ', i, characteristic);

                    //                 for (let j = 0; j < characteristic.length; j++) {
                    //                     this.manager.readCharacteristicForDevice(characteristic[j].deviceID, characteristic[j].serviceUUID, characteristic[j].uuid).then((result) => {  
                    //                         console.log('------- Read Result --------');
                    //                         console.log(result);
                    //                     }).catch((error) => {
                    //                         console.log('----- Cannot Read -----');
                    //                         console.log(error);
                    //                     });

                    //                     this.manager.monitorCharacteristicForDevice(characteristic[j].deviceID, characteristic[j].uuid, (error, result) => {
                    //                         console.log('------ MONITORING Result -------');
                    //                         console.log('index', j);
                    //                         if (error) {
                    //                             console.log(error);
                    //                             return;
                    //                         }

                    //                         console.log(result);
                    //                     });
                    //                 }
                    //             });
                    //         }
                    //     }
                    // }).catch((error) => {
                    //     console.log('Error: ',error);
                    // });

                }).catch(err => {
                    console.log('Error');
                    console.log(err);
                });
            }

        });
    }

    readServiceForDeviceID() {
        if (this.state.deviceId === null) {
            return;
        }
        this.manager.servicesForDevice(this.state.deviceId).then(result => {
            if (result) {
                console.log(result);
                let list = [];
                for (let i = 0; i < result.length; i++) {
                    let data = {
                        id: result[i].id,
                        uuid: result[i].uuid,
                        deviceId: result[i].deviceID
                    }
                    list.push(data);
                }
                this.setState({
                    servicesList: list
                });
            }
        }).catch(err => {
            console.log('Service For Devices Error');
            console.log(err);
        });
    }

    getCharacteristic(item) {
        this.manager.characteristicsForDevice(item.deviceId, item.uuid).then((characteristic) => {
            console.log('----- characteristic -----');
            console.log(characteristic);
            if (characteristic) {
                let list = [];
                for (let i = 0; i < characteristic.length; i++) {
                    let data = {
                        deviceID: characteristic[i].deviceID,
                        id: characteristic[i].id,
                        isIndicateable: characteristic[i].isIndicatable,
                        isNotifiable: characteristic[i].isNotifiable,
                        isNotifying: characteristic[i].isNotifying,
                        isReadable: characteristic[i].isReadable,
                        serviceID: characteristic[i].serviceID,
                        serviceUUID: characteristic[i].serviceUUID,
                        uuid: characteristic[i].uuid,
                        value: characteristic[i].value,
                    }
                    list.push(data);
                }

                this.setState({
                    characteristicsList: list
                });
            }
        }).catch((err) => {
            console.log('read characteristic error : ', err);
        });
    }

    readCharacteristic(item) {
        console.log('click read');
        this.manager.readCharacteristicForDevice(item.deviceID, item.serviceUUID, item.uuid).then((result) => {
            console.log('------- Read Result --------');
            console.log(result);
        }).catch((error) => {
            console.log('----- Cannot Read -----');
            console.log(error);
        });

        
    }

    monitoringCharacteristic (item) {
        console.log('click monitoring');
        this.manager.monitorCharacteristicForDevice(item.deviceID, item.serviceUUID, item.uuid, (error, characteristic) => {
            if (error) {
                console.log('----- Monitoring Error ------');
                console.log(error);
                // this.manager.connectToDevice(item.deviceID).then((result) => {
                //     console.log(result);

                // }).catch(err => {
                //     console.log(err);
                // });
                return;
            }

            console.log('===== Monitoring =====');
            console.log(characteristic);
        });
    }


    serviceComponent() {
        const list = this.state.servicesList.map((item, index) => {
            return (
                <Pressable key={index.toString()}
                    style={{
                        borderWidth: 1
                    }}
                    onPress={() => this.getCharacteristic(item)}>
                    <Text>DEVICE ID: {item.deviceId}</Text>
                    <Text>ID: {item.id}</Text>
                    <Text>UUID: {item.uuid}</Text>
                </Pressable>
            );
        });

        return list;
    }

    characteristicComponent() {
        const list = this.state.characteristicsList.map((item, index) => {
            return (
                <View key={index.toString()}
                    style={{
                        borderWidth: 1,
                        borderColor: 'red'
                    }}
                    // onPress={() => this.readCharacteristic(item)}
                    >
                    <Text>DEVICE ID: {item.deviceID}</Text>
                    <Text>ID: {item.id}</Text>
                    <Text>UUID: {item.uuid}</Text>
                    <Text>SERVICEID: {item.serviceID}</Text>
                    <Text>SERVICEUUID: {item.serviceUUID}</Text>
                    <Text>Readable: {item.isReadable.toString()}</Text>
                    <Text>Value: {item.value}</Text>
                    <Text>isNotifiable: {item.isNotifiable.toString()}</Text>
                    <Text>isNotifying: {item.isNotifying.toString()}</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <Button
                            title={"Read"}
                            onPress={() => this.readCharacteristic(item)}
                        />
                        <Button
                            title={"Monitoring"}
                            onPress={() => this.monitoringCharacteristic(item)}
                        />
                    </View>
                </View>
            );
        });

        return list;
    }

    readCharacteristicComponent() {
        return null;
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
                <Button
                    title={"Scan"}
                    onPress={() => this.scan()}
                />
                <View style={{ marginVertical: 10 }} />
                <Button
                    title={"DeviceID"}
                    onPress={() => this.readServiceForDeviceID()}
                />
                <Text>
                    Devide ID: {this.state.deviceId}
                </Text>
                <ScrollView>
                    {this.serviceComponent()}
                    {this.characteristicComponent()}
                    {this.readCharacteristicComponent()}
                </ScrollView>
            </View>
        );
    }
}