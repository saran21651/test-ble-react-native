/**
 * Sample BLE React Native App
 *
 * @format
 * @flow strict-local
 */

import React, {
    useState,
    useEffect,
} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    NativeModules,
    NativeEventEmitter,
    Button,
    Platform,
    PermissionsAndroid,
    FlatList,
    TouchableHighlight,
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';

import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const App = () => {
    const [isScanning, setIsScanning] = useState(false);
    const peripherals = new Map();
    const [list, setList] = useState([]);


    const startScan = () => {
        console.log('startScan');
        if (!isScanning) {
            BleManager.scan([], 3, true).then((results) => {
                console.log('Scanning...');
                setIsScanning(true);
            }).catch(err => {
                console.error(err);
            });
        }
    }

    const handleStopScan = () => {
        console.log('Scan is stopped');
        setIsScanning(false);
    }

    const handleDisconnectedPeripheral = (data) => {
        let peripheral = peripherals.get(data.peripheral);
        if (peripheral) {
            peripheral.connected = false;
            peripherals.set(peripheral.id, peripheral);
            setList(Array.from(peripherals.values()));
        }
        console.log('Disconnected from ' + data.peripheral);
        console.log(data);
        // testPeripheral({
        //     id: data.peripheral,
        // });
    }

    const handleUpdateValueForCharacteristic = (data) => {
        console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    }

    const retrieveConnected = () => {
        BleManager.getConnectedPeripherals([]).then((results) => {
            if (results.length == 0) {
                console.log('No connected peripherals')
            }
            console.log(results);
            for (var i = 0; i < results.length; i++) {
                var peripheral = results[i];
                peripheral.connected = true;
                peripherals.set(peripheral.id, peripheral);
                setList(Array.from(peripherals.values()));
            }
        });
    }

    const handleDiscoverPeripheral = (peripheral) => {
        console.log('Got ble peripheral', peripheral);
        if (!peripheral.name) {
            peripheral.name = 'NO NAME';
        }
        peripherals.set(peripheral.id, peripheral);
        setList(Array.from(peripherals.values()));
    }

    const testPeripheral = (peripheral) => {
        console.log('===== Peripheral -----');
        console.log(peripheral);
        if (peripheral) {
            if (peripheral.connected) {
                BleManager.disconnect(peripheral.id);
            } else {
                BleManager.connect(peripheral.id).then(() => {
                    let p = peripherals.get(peripheral.id);
                    if (p) {
                        p.connected = true;
                        peripherals.set(peripheral.id, p);
                        setList(Array.from(peripherals.values()));
                    }
                    console.log('Connected to ' + peripheral.id);

                    BleManager.createBond(peripheral.id, '704418').then(() => {
                        console.log("createBond success or there is already an existing one");
                    })
                        .catch((err) => {
                            console.log("fail to bond");
                            console.log(err);
                        });

                    setTimeout(() => {

                        /* Test read current RSSI value */

                        BleManager.retrieveServices(peripheral.id).then((peripheralData) => {
                            console.log('Retrieved peripheral services', peripheralData);

                            let uuid = "00000000-0000-1000-1000-000000000000";
                            let c_uuid = "00000000-0000-1000-1000-000000000011";

                            BleManager.read(peripheral.id, uuid, c_uuid).then((readData) => {
                                console.log('Readdata: ---');
                                console.log(readData);
                            }).catch((error) => {
                                console.log(error);
                            });

                            c_uuid = "00000000-0000-1000-1000-000000000012";

                            BleManager.read(peripheral.id, uuid, c_uuid).then((readData) => {
                                console.log('Readdata: ---');
                                console.log(readData);
                            }).catch((error) => {
                                console.log(error);
                            });

                            // for (let i = 0; i < peripheralData.characteristics.length; i++) {
                            //     let uuid = peripheralData.characteristics[i].service;
                            //     let c_uuid = peripheralData.characteristics[i].characteristic;

                            //     BleManager.read(peripheral.id, uuid, c_uuid).then((readData) => {
                            //         console.log('Readdata: ---');
                            //         console.log(readData);
                            //     });
                            // }
                        });

                        // Test using bleno's pizza example
                        // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
                        /*
                        BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
                          console.log(peripheralInfo);
                          var service = '13333333-3333-3333-3333-333333333337';
                          var bakeCharacteristic = '13333333-3333-3333-3333-333333330003';
                          var crustCharacteristic = '13333333-3333-3333-3333-333333330001';
            
                          setTimeout(() => {
                            BleManager.startNotification(peripheral.id, service, bakeCharacteristic).then(() => {
                              console.log('Started notification on ' + peripheral.id);
                              setTimeout(() => {
                                BleManager.write(peripheral.id, service, crustCharacteristic, [0]).then(() => {
                                  console.log('Writed NORMAL crust');
                                  BleManager.write(peripheral.id, service, bakeCharacteristic, [1,95]).then(() => {
                                    console.log('Writed 351 temperature, the pizza should be BAKED');
                                    
                                    //var PizzaBakeResult = {
                                    //  HALF_BAKED: 0,
                                    //  BAKED:      1,
                                    //  CRISPY:     2,
                                    //  BURNT:      3,
                                    //  ON_FIRE:    4
                                    //};
                                  });
                                });
            
                              }, 500);
                            }).catch((error) => {
                              console.log('Notification error', error);
                            });
                          }, 200);
                        });*/



                    }, 900);
                }).catch((error) => {
                    console.log('Connection error', error);
                });
            }
        }

    }

    useEffect(() => {
        BleManager.start({ showAlert: false });

        bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', handleDiscoverPeripheral);
        bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan);
        bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', handleDisconnectedPeripheral);
        bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', handleUpdateValueForCharacteristic);

        if (Platform.OS === 'android' && Platform.Version >= 23) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                } else {
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                        } else {
                            console.log("User refuse");
                        }
                    });
                }
            });
        }

        return (() => {
            console.log('unmount');
            // bleManagerEmitter.removeEventListener('BleManagerDiscoverPeripheral', handleDiscoverPeripheral);
            // bleManagerEmitter.removeEventListener('BleManagerStopScan', handleStopScan);
            // bleManagerEmitter.removeEventListener('BleManagerDisconnectPeripheral', handleDisconnectedPeripheral);
            // bleManagerEmitter.removeEventListener('BleManagerDidUpdateValueForCharacteristic', handleUpdateValueForCharacteristic);
        })
    }, []);

    const renderItem = (item) => {
        const color = item.connected ? 'green' : '#fff';
        return (
            <TouchableHighlight onPress={() => testPeripheral(item)}>
                <View style={[styles.row, { backgroundColor: color }]}>
                    <Text style={{ fontSize: 12, textAlign: 'center', color: '#333333', padding: 10 }}>{item.name}</Text>
                    <Text style={{ fontSize: 10, textAlign: 'center', color: '#333333', padding: 2 }}>RSSI: {item.rssi}</Text>
                    <Text style={{ fontSize: 8, textAlign: 'center', color: '#333333', padding: 2, paddingBottom: 20 }}>{item.id}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    {global.HermesInternal == null ? null : (
                        <View style={styles.engine}>
                            <Text style={styles.footer}>Engine: Hermes</Text>
                        </View>
                    )}
                    <View style={styles.body}>

                        <View style={{ margin: 10 }}>
                            <Button
                                title={'Scan Bluetooth (' + (isScanning ? 'on' : 'off') + ')'}
                                onPress={() => startScan()}
                            />
                        </View>

                        <View style={{ margin: 10 }}>
                            <Button title="Retrieve connected peripherals" onPress={() => retrieveConnected()} />
                        </View>

                        <View style={{ margin: 10 }}>
                            <Button title="Start Notification" onPress={() => {
                                let p_id = "C8:FD:19:9B:51:79";
                                let c_id = "00000000-0000-1000-1000-000000000012";
                                let u_id = "00000000-0000-1000-1000-000000000000";
                                BleManager.startNotification(p_id, u_id, c_id).then((result) => {
                                    console.log('Start Notificaiton Result');
                                    console.log(result);
                                }).catch((error) => {
                                    console.log('Notification Error ');
                                    console.log(error);
                                });
                            }} />
                        </View>

                        {(list.length == 0) &&
                            <View style={{ flex: 1, margin: 20 }}>
                                <Text style={{ textAlign: 'center' }}>No peripherals</Text>
                            </View>
                        }

                    </View>
                </ScrollView>
                <FlatList
                    data={list}
                    renderItem={({ item }) => renderItem(item)}
                    keyExtractor={item => item.id}
                />
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default App;