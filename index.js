/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
// import App from './src/test-screen';
import App from './src/test-ble-pix';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
